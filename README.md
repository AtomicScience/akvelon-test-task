# Akvelon Test Task
This is a simple REST API, built with the following tech stack:
## Solution stack
- **Framework**
    - Spring Boot
- **Persistence**
    - Spring Data
    - Hibernate
    - PostgreSQL
- **DevOps**
    - Docker
    - Docker Compose
    - GitLab CI
- **Documentation**
    - Swagger/OpenAPI 3.0

## Documentation
API is documented according to OpenAPI 3.0 specification.

This documentation can be found [here](http://testserver.atomicscience.ru:8080/swagger-ui/index.html#/)

## CI/CD
The application has a pipeline, that automatically does the following:
1) Compiles and tests the code using Maven
2) Builds a Docker images and uploads it on DockerHub
3) Runs the image, along with PostgreSQL container on remote VPS using docker-compose

The application is deployed here:
```
testserver.atomicscience.ru
```
## How to try the API?
There are a couple of ways to test this API in action:
### SwaggerHub and already-deployed server
The simplest way is to use an already-deployed instance of the application and to try the methods in project's [documentation](http://testserver.atomicscience.ru:8080/swagger-ui/index.html#/)
### Build and run the project using Docker Compose script
Project provides a Docker Compose script to build the project, and it, as well as the database, can be deployed with a provided docker-compose
```bash
$ git clone https://gitlab.com/AtomicScience/akvelon-test-task.git
$ cd akvelon-test-task
$ docker-compose -f docker-compose-dev.yml up -d
```
After this, the ready-to-use application will be available at `localhost:8080`
