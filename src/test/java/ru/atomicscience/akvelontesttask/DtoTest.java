package ru.atomicscience.akvelontesttask;

import org.junit.jupiter.api.Test;
import org.modelmapper.ModelMapper;
import ru.atomicscience.akvelontesttask.dto.ProjectDto;
import ru.atomicscience.akvelontesttask.dto.TaskDto;
import ru.atomicscience.akvelontesttask.models.Project;
import ru.atomicscience.akvelontesttask.models.Task;

import java.util.Date;
import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class DtoTest {
    private final ModelMapper mapper = new ModelMapper();

    @Test
    public void whenMappingsValidatedExpectValid() {
        mapper.createTypeMap(Project.class, ProjectDto.class);
        mapper.createTypeMap(Task.class, TaskDto.class);
        assertDoesNotThrow(mapper::validate);
    }

    @Test
    public void whenConvertedToDtoExpectValid() {
        List<UUID> listOfTaskIDs = List.of(UUID.randomUUID(), UUID.randomUUID());
        Date completionDate = new Date();

        Project project = new Project();

        project.setId(UUID.randomUUID());
        project.setName("Cool project");
        project.setTasksIds(listOfTaskIDs);
        project.setCompletionDate(completionDate);

        ProjectDto dto = mapper.map(project, ProjectDto.class);

        assertEquals(project.getName(), dto.getName());
        assertEquals(project.getId(), dto.getId());
        assertEquals(project.getCompletionDate(), dto.getCompletionDate());
        assertEquals(project.getTasksIds(), dto.getTasksIds());

        project = mapper.map(dto, Project.class);

        assertEquals(project.getName(), dto.getName());
        assertEquals(project.getId(), dto.getId());
        assertEquals(project.getCompletionDate(), dto.getCompletionDate());
        assertEquals(project.getTasksIds(), dto.getTasksIds());
    }
}
