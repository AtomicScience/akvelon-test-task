package ru.atomicscience.akvelontesttask.models;

public enum ProjectStatus {
    NOT_STARTED, ACTIVE, COMPLETED
}
