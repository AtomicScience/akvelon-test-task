package ru.atomicscience.akvelontesttask.models;

public enum TaskStatus {
    TODO, IN_PROGRESS, DONE
}
