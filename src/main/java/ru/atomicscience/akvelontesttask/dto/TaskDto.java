package ru.atomicscience.akvelontesttask.dto;

import lombok.Getter;
import lombok.Setter;
import ru.atomicscience.akvelontesttask.models.TaskStatus;

import java.util.UUID;

@Getter
@Setter
public class TaskDto {
    UUID id;

    String name;
    String description;

    TaskStatus status;
    int priority;
}
