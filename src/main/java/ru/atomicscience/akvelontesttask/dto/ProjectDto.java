package ru.atomicscience.akvelontesttask.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.deser.std.UUIDDeserializer;
import lombok.Getter;
import lombok.Setter;
import ru.atomicscience.akvelontesttask.models.ProjectStatus;

import java.util.Date;
import java.util.List;
import java.util.UUID;

@Getter
@Setter
public class ProjectDto {
    @JsonDeserialize(using = UUIDDeserializer.class, contentUsing = UUIDDeserializer.class)
    UUID id;
    String name;
    String description;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    Date startDate;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    Date completionDate;

    ProjectStatus status;
    int priority;
    List<UUID> tasksIds;
}
