package ru.atomicscience.akvelontesttask.controllers;

import com.google.common.collect.Streams;
import org.modelmapper.ModelMapper;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.atomicscience.akvelontesttask.dao.ProjectsCrudRepository;
import ru.atomicscience.akvelontesttask.dao.TasksCrudRepository;
import ru.atomicscience.akvelontesttask.dto.ProjectDto;
import ru.atomicscience.akvelontesttask.models.Project;
import java.util.*;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/projects")
public class ProjectController {
    private final ProjectsCrudRepository projectsCrudRepository;
    private final TasksCrudRepository tasksCrudRepository;
    private final ModelMapper modelMapper;

    public ProjectController(ProjectsCrudRepository projectsCrudRepository,
                             TasksCrudRepository tasksCrudRepository,
                             ModelMapper modelMapper) {
        this.projectsCrudRepository = projectsCrudRepository;
        this.tasksCrudRepository = tasksCrudRepository;
        this.modelMapper = modelMapper;
    }

    @GetMapping
    public List<ProjectDto> getProjects() {
        return Streams.stream(projectsCrudRepository
                .findAll())
                .map((val) -> modelMapper.map(val, ProjectDto.class))
                .collect(Collectors.toList());
    }

    @PostMapping
    public ResponseEntity<String> addProject(@RequestBody ProjectDto project) {
        if(Objects.isNull(project.getName())) {
            return ResponseEntity.badRequest().body("The project is missing name");
        }

        if(Objects.nonNull(project.getId())) {
            return ResponseEntity.badRequest().body("Creation of projects with specific IDs is forbidden");
        }

        if(Objects.nonNull(project.getTasksIds())) {
            for (UUID taskId : project.getTasksIds()) {
                if (!tasksCrudRepository.existsById(taskId))
                    return ResponseEntity.badRequest().body("Cannot add non-existent task with ID: " + taskId);
            }
        }

        Project savedProject = projectsCrudRepository.save(modelMapper.map(project, Project.class));

        return ResponseEntity.ok(savedProject.getId().toString());
    }

    @GetMapping("/{id}")
    public ResponseEntity<ProjectDto> getSingleProject(@PathVariable("id") UUID id) {
        Optional<Project> foundProject = projectsCrudRepository.findById(id);

        return foundProject.isEmpty() ?
                ResponseEntity.notFound().build() :
                ResponseEntity.ok(modelMapper.map(foundProject.get(), ProjectDto.class));
    }

    @PutMapping("/{id}")
    ResponseEntity<Object> updateProject(@PathVariable("id") UUID id, @RequestBody ProjectDto newProject) {
        if(!projectsCrudRepository.existsById(id)) {
            return ResponseEntity.notFound().build();
        }

        if(Objects.isNull(newProject.getName())) {
            return ResponseEntity.badRequest().body("The project is missing name");
        }

        if(Objects.nonNull(newProject.getId())) {
            return ResponseEntity.badRequest().body("Change of ID is forbidden");
        }

        for(UUID taskId : newProject.getTasksIds()) {
            if(!tasksCrudRepository.existsById(taskId))
                return ResponseEntity.badRequest().body("Cannot add non-existent task with ID:" + taskId);
        }

        Project savedProject = projectsCrudRepository.save(modelMapper.map(newProject, Project.class));

        return ResponseEntity.ok(modelMapper.map(savedProject, Project.class));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteSingleProject(@PathVariable("id") UUID id) {
        Optional<Project> possibleProject = projectsCrudRepository.findById(id);

        if(possibleProject.isEmpty()) {
            return ResponseEntity.notFound().build();
        }

        Project project = possibleProject.get();

        tasksCrudRepository.deleteAllById(project.getTasksIds());
        projectsCrudRepository.delete(project);

        return ResponseEntity.ok().build();
    }
}
