package ru.atomicscience.akvelontesttask.controllers;

import com.google.common.collect.Streams;
import org.modelmapper.ModelMapper;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.Nullable;
import org.springframework.web.bind.annotation.*;
import ru.atomicscience.akvelontesttask.dao.ProjectsCrudRepository;
import ru.atomicscience.akvelontesttask.dao.TasksCrudRepository;
import ru.atomicscience.akvelontesttask.dto.TaskDto;
import ru.atomicscience.akvelontesttask.models.Project;
import ru.atomicscience.akvelontesttask.models.Task;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/tasks")
public class TasksController {
    private final ProjectsCrudRepository projectsCrudRepository;
    private final TasksCrudRepository tasksCrudRepository;
    private final ModelMapper modelMapper;

    public TasksController(ProjectsCrudRepository projectsCrudRepository,
                           TasksCrudRepository tasksCrudRepository,
                           ModelMapper modelMapper) {
        this.projectsCrudRepository = projectsCrudRepository;
        this.tasksCrudRepository = tasksCrudRepository;
        this.modelMapper = modelMapper;
    }

    @GetMapping
    public List<TaskDto> getTasks() {
        return Streams.stream(tasksCrudRepository.findAll())
                .map((val) -> modelMapper.map(val, TaskDto.class))
                .collect(Collectors.toList());
    }

    @SuppressWarnings("OptionalGetWithoutIsPresent")
    @GetMapping("/{id}")
    public ResponseEntity<TaskDto> getSingleTask(@PathVariable("id") UUID id) {
        if(!tasksCrudRepository.existsById(id)) return ResponseEntity.notFound().build();

        return ResponseEntity.ok(modelMapper.map(tasksCrudRepository.findById(id).get(), TaskDto.class));
    }

    @PostMapping
    public ResponseEntity<String> addTask(@RequestBody TaskDto task, @RequestParam @Nullable UUID parentProjectId) {
        if(Objects.isNull(task.getName())) {
            return ResponseEntity.badRequest().body("The task is missing name");
        }

        if(Objects.nonNull(task.getId())) {
            return ResponseEntity.badRequest().body("Creation of tasks with specific IDs is forbidden");
        }

        Task savedTask = tasksCrudRepository.save(modelMapper.map(task, Task.class));

        if(parentProjectId != null) {
            Optional<Project> possibleProject = projectsCrudRepository.findById(parentProjectId);

            if(possibleProject.isEmpty())
                return ResponseEntity.badRequest().body("Non-existent project");

            Project parentProject = possibleProject.get();

            parentProject.getTasksIds().add(savedTask.getId());

            projectsCrudRepository.save(parentProject);
        }

        return ResponseEntity.ok(savedTask.getId().toString());
    }

    @PutMapping("/{id}")
    public ResponseEntity<Object> putTask(@PathVariable("id") UUID id, @RequestBody TaskDto task) {
        if(!tasksCrudRepository.existsById(id)) {
            return ResponseEntity.notFound().build();
        }

        if(Objects.isNull(task.getName())) {
            return ResponseEntity.badRequest().body("The task is missing name");
        }

        if(Objects.nonNull(task.getId())) {
            return ResponseEntity.badRequest().body("Change of IDs is forbidden");
        }

        Task savedTask = tasksCrudRepository.save(modelMapper.map(task, Task.class));

        return ResponseEntity.ok(modelMapper.map(savedTask, TaskDto.class));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteTask(@PathVariable("id") UUID taskId) {
        if(!tasksCrudRepository.existsById(taskId)) return ResponseEntity.notFound().build();

        Optional<Project> possibleParentProject = projectsCrudRepository.findProjectByTasksIdsContaining(taskId);

        if(possibleParentProject.isPresent()) {
            Project project = possibleParentProject.get();
            project.getTasksIds().remove(taskId);
            projectsCrudRepository.save(project);
        }

        tasksCrudRepository.deleteById(taskId);

        return ResponseEntity.ok().build();
    }
}
