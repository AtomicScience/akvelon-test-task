package ru.atomicscience.akvelontesttask.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.core.io.ResourceLoader;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.atomicscience.akvelontesttask.dao.ProjectsCrudRepository;
import ru.atomicscience.akvelontesttask.dao.TasksCrudRepository;
import ru.atomicscience.akvelontesttask.models.Project;
import ru.atomicscience.akvelontesttask.models.Task;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

@RestController
@RequestMapping("/debug")
public class DebugController {
    private final ProjectsCrudRepository projectsRepository;
    private final TasksCrudRepository tasksRepository;
    private final ResourceLoader resourceLoader;

    public DebugController(ProjectsCrudRepository projectsRepository,
                           TasksCrudRepository tasksRepository,
                           ResourceLoader resourceLoader) {
        this.projectsRepository = projectsRepository;
        this.tasksRepository = tasksRepository;
        this.resourceLoader = resourceLoader;
    }

    // Had to disable this endpoint due to a weird bug
    // @PostMapping("/populateDatabase")
    public ResponseEntity<Void> populateDatabase() throws IOException {
        projectsRepository.deleteAll();
        tasksRepository.deleteAll();

        List<Task> tasks = loadListFromFile("exampleTasks.json", Task.class);
        List<Project> projects = loadListFromFile("exampleProjects.json", Project.class);

        for(int i = 0; i < projects.size(); i++) {
            Task task = tasksRepository.save(tasks.get(i));
            Project project = projects.get(i);

            project.getTasksIds().add(task.getId());

            projectsRepository.save(project);
        }

        return ResponseEntity.ok().build();
    }

    @PostMapping("/resetStorage")
    public ResponseEntity<Void> resetStorage() {
        projectsRepository.deleteAll();
        tasksRepository.deleteAll();

        return ResponseEntity.ok().build();
    }

    private <T> List<T> loadListFromFile (String filename, Class<T> type) throws IOException {
        ObjectMapper mapper = new ObjectMapper();

        InputStream stream = resourceLoader.getResource("classpath:" + filename).getInputStream();

        return mapper.readValue(
                stream,
                mapper.getTypeFactory().constructCollectionType(List.class, type)
        );
    }
}
