package ru.atomicscience.akvelontesttask.dao;

import org.springframework.data.repository.CrudRepository;
import ru.atomicscience.akvelontesttask.models.Task;

import java.util.UUID;

public interface TasksCrudRepository extends CrudRepository<Task, UUID> {
}
