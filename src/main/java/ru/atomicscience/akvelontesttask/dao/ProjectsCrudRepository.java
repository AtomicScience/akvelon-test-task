package ru.atomicscience.akvelontesttask.dao;

import org.springframework.data.repository.CrudRepository;
import ru.atomicscience.akvelontesttask.models.Project;
import ru.atomicscience.akvelontesttask.models.Task;

import java.util.Optional;
import java.util.UUID;

public interface ProjectsCrudRepository extends CrudRepository<Project, UUID> {
    Optional<Project> findProjectByTasksIdsContaining(UUID id);
}
